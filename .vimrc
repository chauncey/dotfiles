" ~~~~~ General ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set nocompatible
syntax on " syntax highlighting on

" Use pathogen to easily modify the runtime path to include all
" plugins under the ~/.vim/bundle directory
filetype off
call pathogen#helptags()
call pathogen#runtime_append_all_bundles()
filetype plugin indent on

"Set mapleader
let mapleader = ","

set noea " set no equal always for splits
set autoindent
set smartindent
set smartcase
set expandtab
set ts=4
set softtabstop=4
set sw=4
set nojoinspaces
set modeline
set hidden
set history=1000
set undolevels=1000
set wildignore=*.swp,*.bak,*.pyc,*.class
set title
set pastetoggle=<F2> " paste mode for sane right click pasting
set lsp=0 " space it out a little more (easier to read)
set wildmenu " turn on wild menu
set ruler " Always show current positions along the bottom
set number " turn on line numbers
set lz " do not redraw while running macros (much faster) (LazyRedraw)
set hid " you can change buffer without saving
set whichwrap+=<,>,h,l  " backspace and cursor keys wrap to
set mouse=a " use mouse everywhere
"set fillchars=vert:\ ,stl:\ ,stlnc:
set backspace=indent,eol,start
set showmatch " show matching brackets
set mat=5 " how many tenths of a second to blink matching brackets for
set hlsearch " highlight searched for phrases
set incsearch " BUT do highlight as you type you search phrase
set t_vb= " stop with the beeps

set laststatus=2 " always show the status line

"set statusline=%F%m%r%h%w\ \ FORMAT=%{&ff}\ \ TYPE=%Y\ \ POS=%04l\ \|\ %04v\ \ [%p%%]\ \ LEN=%L
set statusline=%{fugitive#statusline()}%{hostname()}:%F%m%r%h%w\ \ TYPE=%Y\ \ POS=%04l\ \|\ %04v\ \ LEN=%L

set background=dark
set shortmess=filnxtToO
set t_Co=256


" ~~~~~ Mappings  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
inoremap jj <Esc>
map <right> <ESC>:MBEbn<RETURN> " right arrow (normal mode) switches buffers  (excluding minibuf)
map <left> <ESC>:MBEbp<RETURN> " left arrow (normal mode) switches buffers (excluding minibuf)
"map <up> <ESC>:Sex<RETURN><ESC><C-W><C-W> " up arrow (normal mode) brings up a file list
map <A-i> i <ESC>r " alt-i (normal mode) inserts a single char, and then switches back to normal
nnoremap <silent> <F11> :YRShow<CR>
map T :TaskList<CR>
map <F7> :TlistToggle<CR>
map <F8> :NERDTree<CR>
noremap <Leader>n :NERDTreeToggle<CR>
noremap <Leader>v :vsp^M^W^W<cr>
noremap <Leader>r :vertical res
noremap <Leader>fm :set foldmethod=manual<cr>
" No longer have to hit Shift :w for save
nnoremap ; :
nmap <silent> ,/ :nohlsearch<CR> " clear search highlights
" don't skip over wrapped line
nnoremap j gj
nnoremap k gk
nnoremap <leader><space> :noh<cr> " clear highlight
nnoremap <leader>t    :set t_Co=8<cr> :colorscheme ir_black<cr> " bloody Terminal.app and it's broken color support
nnoremap <F5> :GundoToggle<cr>

cmap w!! w !sudo tee % >/dev/null " forgot to open file with root, juse w!!

" Quickly edit/reload the vimrc file
nmap <silent> <leader>ev :e $MYVIMRC<CR>
nmap <silent> <leader>sv :so $MYVIMRC<CR>

"Smart way to move btw. windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l
map <leader>cd :cd %:p:h<cr>
nnoremap <leader>w <C-w>v<C-w>l

"Cut/Copy/Paste
vmap <C-c> "+yi
vmap <C-x> "+c
vmap <C-v> c<ESC>"+p
imap <C-v> <ESC>"+pa


" ~~~~~ NERDTree ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
map <F8> :NERDTree<CR>
noremap <Leader>n :NERDTreeToggle<CR>
let NERDTreeMouseMode=1 " Single click for everything


" ~~~~~ Buffer  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
autocmd BufEnter * :syntax sync fromstart " ensure every file does syntax highlighting (full)
au BufNewFile,BufRead *.pt :set ft=xml " all my .pt files ARE xml
au BufNewFile,BufRead *.txt :set ft=txt
autocmd BufWritePre *.py :%s/\s\+$//e "
autocmd BufWritePre *.js :%s/\s\+$//e "
autocmd BufWritePre *.pt :%s/\s\+$//e "
autocmd BufWritePre *.xml :%s/\s\+$//e "

" highligth > 80 chars line
"au BufWinEnter * let w:m1=matchadd('Search', '\%<81v.\%>77v', -1)
"au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1)

" ~~~~~ Omni  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set completeopt=menu
au Filetype * let &l:ofu = (len(&ofu) ? &ofu : 'syntaxcomplete#Complete')

autocmd FileType python set omnifunc=pythoncomplete#Complete
autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
autocmd FileType css set omnifunc=csscomplete#CompleteCSS
autocmd FileType xml set omnifunc=xmlcomplete#CompleteTags
autocmd FileType php set omnifunc=phpcomplete#CompletePHP
autocmd FileType c set omnifunc=ccomplete#Complete
"let g:SuperTabDefaultCompletionType = "<C-X><C-O>" " supertab 
let g:SuperTabDefaultCompletionType = "context" " supertab 

" ~~~~~ Filetype settings ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
autocmd BufRead *.py let python_highlight_builtins = 1
autocmd BufRead *.py let python_highlight_numbers = 1
au FileType html,xml,xhtml,pt,javascript,css setl shiftwidth=2 tabstop=2 softtabstop=2 
au FileType python,java,php setl shiftwidth=4 tabstop=4


" ~~~~~ For javascript folding  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function! JavaScriptFold()
    setl foldmethod=syntax
    setl foldlevelstart=1
    syn region foldBraces start=/{/ end=/}/ transparent fold keepend extend

    function! FoldText()
        return "+--  " . substitute(getline(v:foldstart), '{.*', '{...}', '')
    endfunction
    setl foldtext=FoldText()
endfunction
au FileType javascript call JavaScriptFold()
au FileType javascript setl fen


" ~~~~~ Python  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
python << EOF
import os
import sys
import vim
for p in sys.path:
    if os.path.isdir(p):
        vim.command(r"set path+=%s" % (p.replace(" ", r"\ ")))
EOF


" ~~~~~ GUI and OS specific stuff  ~~~~~~~~~~~~~~~~~~~~~~~~~
let os = substitute(system('uname'), "\n", "", "")
if os == "AIX"
    colorscheme jellycc 
	au BufRead,BufNewFile *.lst set filetype=bbx
    au FileType bbx setl nonumber 
	nmap <F4> :execute BBXListSave()<CR>
	nmap bo :execute BBXLoadInput()<CR>
	nmap bs :execute BBXSave()<CR>
endif

if os == "Linux"
    colorscheme jellycc "colorscheme solarized "colorscheme ir_black
	au BufRead,BufNewFile *.lst set filetype=vb
    set guioptions-=m
    set guioptions-=T
endif

if &t_Co >= 256 || has("gui_running")
    " No toolbar
    set go-=T
    " Omni menu colors
    if has("gui_running")
        hi Pmenu guibg=#333333
        hi PmenuSel guibg=#555555 guifg=#ffffff
    else
        hi Pmenu ctermbg=238
    endif
    " Mac specific 
    if has("gui_mac") || has("gui_macvim")
        set guioptions=egmrt
        colorscheme ir_black 
        try
            set transparency=2
        catch
        endtry
    endif
else
    colorscheme ir_black
endif

if &term =~ "xterm"
    " restore screen after quitting
    " set t_ti=ESC7ESC[rESC[?47h t_te=ESC[?47lESC8
    if has("terminfo")
        let &t_Sf="\ESC[3%p1%dm"
        let &t_Sb="\ESC[4%p1%dm"
    else
        let &t_Sf="\ESC[3%dm"
        let &t_Sb="\ESC[4%dm"
    endif
endif
        

